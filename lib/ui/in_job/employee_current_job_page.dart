import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';

class EmployeeCurrentJobPage extends StatefulWidget {
  const EmployeeCurrentJobPage({Key key}) : super(key: key);

  @override
  _CurrentJobState createState() => new _CurrentJobState();
}

class _CurrentJobState extends State<EmployeeCurrentJobPage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(title: new Text('New Job')),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext buildContext) {
    return Stack(
        alignment: Alignment.center, children: _buildContent(buildContext));
  }

  List<Widget> _buildContent(BuildContext buildContext) {
    // todo use real data
    return [_buildQrImage("1234567890")];
  }

  QrImage _buildQrImage(String input) {
    return new QrImage(
      data: input,
      size: 200.0,
    );
  }
}
