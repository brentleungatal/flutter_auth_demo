import 'package:flutter/material.dart';
import 'package:testing/ui/welcome/welcome_page.dart';

void main() {
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
        title: 'Freelancer App',
        theme: new ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: new WelcomePage());
  }
}
