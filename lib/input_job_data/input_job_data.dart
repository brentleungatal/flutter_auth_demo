import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class InputJobDataPage extends StatefulWidget {
  const InputJobDataPage({Key key}) : super(key: key);

  @override
  _InputJobDataState createState() => new _InputJobDataState();
}

class _InputJobDataState extends State<InputJobDataPage> {
  final jobTextEditingController = TextEditingController();
  final wageTextEditingController = TextEditingController();

  String job = ''; // title
  BigInt wage = new BigInt.from(0); // wage
  String employer = '';
  String freelancer = '';

  final _formKey = GlobalKey<FormState>();

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    jobTextEditingController.dispose();
    wageTextEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: AppBar(
          title: Text('Create a new Job'),
        ),
        body: _buildBody(context));
  }

  Widget _buildBody(BuildContext buildContext) {
    return Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            TextFormField(
              decoration: const InputDecoration(
                  hintText: 'What`s the job?', labelText: 'Job Name *'),
              validator: (value) {
                if (value.isEmpty) {
                  return 'Please enter some text';
                }
              },
              controller: jobTextEditingController,
            ),
            TextFormField(
              decoration: const InputDecoration(
                  hintText: 'What`s the wage?', labelText: 'Wage *'),
              validator: (value) {
                if (value.isEmpty) {
                  return 'Please enter some text';
                }
              },
              controller: wageTextEditingController,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 16.0),
              child: RaisedButton(
                onPressed: () {
                  // Validate will return true if the form is valid, or false if
                  // the form is invalid.
                  if (_formKey.currentState.validate()) {
                    // If the form is valid, we want to show a Snackbar
                    print(
                        'job title: ${jobTextEditingController.text}, wage ${wageTextEditingController.text}');

                    Firestore.instance.collection('jobs').document().setData({
                      'job': jobTextEditingController.text,
                      'wage': wageTextEditingController.text
                    });
                  }
                },
                child: Text('Create Job!'),
              ),
            ),
          ],
        ));
  }
}
