import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:testing/model/job.dart';

final CollectionReference jobCollection = Firestore.instance.collection('jobs');

class FirebaseFirestoreService {
  static final FirebaseFirestoreService _instance =
  new FirebaseFirestoreService.internal();

  factory FirebaseFirestoreService() => _instance;

  FirebaseFirestoreService.internal();

  Future<Job> createJob(String title,
      String description,
      String employer,
      String employee,
      String expectedStartTime,
      String expectedEndTime,
      String expectedDuration,
      String actualStartTime,
      String actualEndTime,
      String actualDuration,
      String wage) async {
    final TransactionHandler createTransaction = (Transaction tx) async {
      final DocumentSnapshot ds = await tx.get(jobCollection.document());

      final Job job = new Job(
          ds.documentID,
          title,
          description,
          employer,
          employee,
          expectedStartTime,
          expectedEndTime,
          expectedDuration,
          actualStartTime,
          actualEndTime,
          actualDuration,
          wage);
      final Map<String, dynamic> data = job.toMap();

      await tx.set(ds.reference, data);

      return data;
    };

    return Firestore.instance.runTransaction(createTransaction).then((mapData) {
      return Job.fromMap(mapData);
    }).catchError((error) {
      print('error: $error');
      return null;
    });
  }

  Stream<QuerySnapshot> getJobList({int offset, int limit}) {
    Stream<QuerySnapshot> snapshots = jobCollection.snapshots();

    if (offset != null) {
      snapshots = snapshots.skip(offset);
    }

    if (limit != null) {
      snapshots = snapshots.take(limit);
    }

    return snapshots;
  }

  Future<dynamic> updateJob(Job job) async {
    final TransactionHandler updateTransaction = (Transaction tx) async {
      final DocumentSnapshot ds = await tx.get(jobCollection.document(job.id));

      await tx.update(ds.reference, job.toMap());
      return {'updated': true};
    };

    return Firestore.instance
        .runTransaction(updateTransaction)
        .then((result) => result['updated'])
        .catchError((error) {
      print('error: $error');
      return false;
    });
  }

  Future<dynamic> deleteJob(String id) async {
    final TransactionHandler deleteTransaction = (Transaction tx) async {
      final DocumentSnapshot ds = await tx.get(jobCollection.document(id));

      await tx.delete(ds.reference);
      return {'deleted': true};
    };

    return Firestore.instance
        .runTransaction(deleteTransaction)
        .then((result) => result['deleted'])
        .catchError((error) {
      print('error: $error');
      return false;
    });
  }
}
