import 'package:flutter/material.dart';
import 'package:testing/ui/user/activity_logs_page.dart';

void main() => runApp(
  MaterialApp(
    title: 'Returning Data',
    home: ListViewJob(),
  ),
);
