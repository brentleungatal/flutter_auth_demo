import 'package:flutter/material.dart';

class Summary extends StatefulWidget {
  const Summary({Key key}) : super(key: key);

  @override
  _SummaryState createState() => new _SummaryState();
}

class _SummaryState extends State<Summary> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Job Summary'),
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext buildContext) {
    return Column(children: _buildContent(buildContext));
  }

  List<Widget> _buildContent(BuildContext buildContext) {
    return [
      Text(
        'Title',
        style: TextStyle(color: Colors.black, fontSize: 18.0),
      )
    ];
  }
}
