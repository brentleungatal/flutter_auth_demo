import 'package:flutter/material.dart';
import 'package:testing/model/job.dart';
import 'package:testing/service/firebase_firestore_service.dart';

class JobScreen extends StatefulWidget {
  final Job job;
  JobScreen(this.job);

  @override
  State<StatefulWidget> createState() => new _JobScreenState();
}

class _JobScreenState extends State<JobScreen> {
  FirebaseFirestoreService db = new FirebaseFirestoreService();

  TextEditingController _titleController;
  TextEditingController _descriptionController;
  TextEditingController _employerController;
  TextEditingController _employeeController;
  TextEditingController _expectedStartTimeController;
  TextEditingController _expectedEndTimeController;
  TextEditingController _expectedDurationController;
  TextEditingController _actualStartTimeController;
  TextEditingController _actualEndTimeController;
  TextEditingController _actualDurationController;
  TextEditingController _wageController;

  @override
  void initState() {
    super.initState();

    _titleController = new TextEditingController(text: widget.job.title);
    _descriptionController =
    new TextEditingController(text: widget.job.description);
    _employerController = new TextEditingController(text: widget.job.employer);
    _employeeController = new TextEditingController(text: widget.job.employee);
    _expectedStartTimeController =
    new TextEditingController(text: widget.job.expectedStartTime);
    _expectedEndTimeController =
    new TextEditingController(text: widget.job.expectedEndTime);
    _expectedDurationController =
    new TextEditingController(text: widget.job.expectedDuration);
    _actualStartTimeController =
    new TextEditingController(text: widget.job.actualStartTime);
    _actualEndTimeController =
    new TextEditingController(text: widget.job.actualEndTime);
    _actualDurationController =
    new TextEditingController(text: widget.job.actualDuration);
    _wageController = new TextEditingController(text: widget.job.wage);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Job')),
      body: Container(
        margin: EdgeInsets.all(15.0),
        alignment: Alignment.center,
        child: ListView(
          children: <Widget>[
            TextField(
              controller: _titleController,
              decoration: InputDecoration(labelText: 'Title'),
            ),
            Padding(padding: new EdgeInsets.all(5.0)),
            TextField(
              controller: _descriptionController,
              decoration: InputDecoration(labelText: 'Description'),
            ),
            Padding(padding: new EdgeInsets.all(5.0)),
            TextField(
              controller: _employerController,
              decoration: InputDecoration(labelText: 'Employer'),
            ),
            Padding(padding: new EdgeInsets.all(5.0)),
            TextField(
              controller: _employeeController,
              decoration: InputDecoration(labelText: 'Employee'),
            ),
            Padding(padding: new EdgeInsets.all(5.0)),
            TextField(
              controller: _expectedStartTimeController,
              decoration: InputDecoration(labelText: 'Expected Start Time'),
            ),
            Padding(padding: new EdgeInsets.all(5.0)),
            TextField(
              controller: _expectedEndTimeController,
              decoration: InputDecoration(labelText: 'Expected End Time'),
            ),
            Padding(padding: new EdgeInsets.all(5.0)),
            TextField(
              controller: _expectedDurationController,
              decoration: InputDecoration(labelText: 'Expected Duration'),
            ),
            Padding(padding: new EdgeInsets.all(5.0)),
            TextField(
              controller: _actualStartTimeController,
              decoration: InputDecoration(labelText: 'Actual Start Time'),
            ),
            Padding(padding: new EdgeInsets.all(5.0)),
            TextField(
              controller: _actualEndTimeController,
              decoration: InputDecoration(labelText: 'Actual End Time'),
            ),
            Padding(padding: new EdgeInsets.all(5.0)),
            TextField(
              controller: _actualDurationController,
              decoration: InputDecoration(labelText: 'Actual Duration'),
            ),
            Padding(padding: new EdgeInsets.all(5.0)),
            TextField(
              controller: _wageController,
              decoration: InputDecoration(labelText: 'Wage'),
            ),
            Padding(padding: new EdgeInsets.all(5.0)),
            RaisedButton(
              child: (widget.job.id != null) ? Text('Update') : Text('Add'),
              onPressed: () {
                if (widget.job.id != null) {
                  db
                      .updateJob(Job(
                    widget.job.id,
                    _titleController.text,
                    _descriptionController.text,
                    _employerController.text,
                    _employeeController.text,
                    _expectedStartTimeController.text,
                    _expectedEndTimeController.text,
                    _expectedDurationController.text,
                    _actualStartTimeController.text,
                    _actualEndTimeController.text,
                    _actualDurationController.text,
                    _wageController.text,
                  ))
                      .then((_) {
                    Navigator.pop(context);
                  });
                } else {
                  db
                      .createJob(
                    _titleController.text,
                    _descriptionController.text,
                    _employerController.text,
                    _employeeController.text,
                    _expectedStartTimeController.text,
                    _expectedEndTimeController.text,
                    _expectedDurationController.text,
                    _actualStartTimeController.text,
                    _actualEndTimeController.text,
                    _actualDurationController.text,
                    _wageController.text,
                  )
                      .then((_) {
                    Navigator.pop(context);
                  });
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
