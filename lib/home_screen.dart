import 'package:flutter/material.dart';

import './countdown_screen.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      children: <Widget>[
        Expanded(child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Freelancer App',
              style:TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 36,
              )
              ),
          ],
        )),
        Expanded(
          flex: 2,
          child: Row(
            children: <Widget>[
              Expanded(child: ButtonContainer()),
              Expanded(child: ButtonContainer(
                icon: 
                  Icons.border_outer,
                  text: 'Scan QR CODE'
                
              )),
            ],
          ),
        ),
        Expanded(
          flex: 4,
          child: Row(children: <Widget>[Expanded(child: ButtonContainer())]),
        )
      ],
    );
  }
}

class ButtonContainer extends StatelessWidget {
  final String text;
  final IconData icon;

  ButtonContainer({
    this.text: 'ADD A JOB',
    this.icon: Icons.add_circle_outline,
  });

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return RaisedButton(
      color: Colors.white,
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => CountdownScreen()),
          );
        },
        child: Container(
            width: 500,
            height: 500,
            child: Container(
              margin: const EdgeInsets.all(10.0),
              // color: const Color(0xFF00FF00),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [Icon(icon, size: 120), Text(text)]),
              height: 100,
              decoration: BoxDecoration(
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.all(Radius.circular(10)),
                color: Colors.grey[400],
              ),
            )));
  }
}
