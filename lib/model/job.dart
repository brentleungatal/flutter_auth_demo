class Job {
  String _id;
  String _title;
  String _description;
  String _employer;
  String _employee;
  String _expectedStartTime;
  String _expectedEndTime;
  String _expectedDuration;
  String _actualStartTime;
  String _actualEndTime;
  String _actualDuration;
  String _wage;

  Job(this._id,
      this._title,
      this._description,
      this._employer,
      this._employee,
      this._expectedStartTime,
      this._expectedEndTime,
      this._expectedDuration,
      this._actualStartTime,
      this._actualEndTime,
      this._actualDuration,
      this._wage);

  Job.map(dynamic obj) {
    this._id = obj['id'];
    this._title = obj['title'];
    this._description = obj['description'];
    this._employer = obj['employer'];
    this._employee = obj['employee'];
    this._expectedStartTime = obj['expectedStartTime'];
    this._expectedEndTime = obj['expectedEndTime'];
    this._expectedDuration = obj['expectedDuration'];
    this._actualStartTime = obj['actualStartTime'];
    this._actualEndTime = obj['actualEndTime'];
    this._actualDuration = obj['actualDuration'];
    this._wage = obj['wage'];
  }

  String get id => _id;
  String get title => _title;
  String get description => _description;
  String get employer => _employer;
  String get employee => _employee;
  String get expectedStartTime => _expectedStartTime;
  String get expectedEndTime => _expectedEndTime;
  String get expectedDuration => _expectedDuration;
  String get actualStartTime => _actualStartTime;
  String get actualEndTime => _actualEndTime;
  String get actualDuration => _actualDuration;
  String get wage => _wage;

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    if (_id != null) {
      map['id'] = _id;
    }
    map['title'] = _title;
    map['description'] = _description;
    map['employer'] = _employer;
    map['employee'] = _employee;
    map['expectedStartTime'] = _expectedStartTime;
    map['expectedEndTime'] = _expectedEndTime;
    map['expectedDuration'] = _expectedDuration;
    map['actualStartTime'] = _actualStartTime;
    map['actualEndTime'] = _actualEndTime;
    map['actualDuration'] = _actualDuration;
    map['wage'] = _wage;

    return map;
  }

  Job.fromMap(Map<String, dynamic> map) {
    this._id = map['id'];
    this._title = map['title'];
    this._description = map['description'];
    this._employer = map['employer'];
    this._employee = map['employee'];
    this._expectedStartTime = map['expectedStartTime'];
    this._expectedEndTime = map['expectedEndTime'];
    this._expectedDuration = map['expectedDuration'];
    this._actualStartTime = map['actualStartTime'];
    this._actualEndTime = map['actualEndTime'];
    this._actualDuration = map['actualDuration'];
    this._wage = map['wage'];
  }
}