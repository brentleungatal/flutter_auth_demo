import 'dart:core';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:testing/input_job_data/input_job_data.dart';
import 'package:testing/ui/summary/summary.dart';

import './home_screen.dart';

class Home extends StatefulWidget {
  const Home({Key key, @required this.user}) : super(key: key);
  final FirebaseUser user;
  @override
  _HomeState createState() => new _HomeState();
}

class _HomeState extends State<Home> {
  String role;
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: HomeScreen(),
      // appBar: new AppBar(
      //   title: new Text('Freelancer App Home'),
      // ),
      // body: _buildBody(context),
      // floatingActionButton: FloatingActionButton(
      //   onPressed: () {
      //     navigateToCurrentJobPage();
      //   },
      //   child: Icon(
      //     Icons.add,
      //   ),
      //   backgroundColor: Colors.pink,
      // ),
      // new Text('User ID: ${widget.user.uid}',
      //style: new TextStyle(fontSize: 20.0)),
    );
  }

  Widget _buildBody(BuildContext buildContext) {
    return Stack(children: <Widget>[
      new Text(widget.user.uid),
      _buildFirestoreStreamList(buildContext),
    ]);
  }

  Widget _buildFirestoreStreamList(BuildContext context) {
    Stream<QuerySnapshot> jobsWhereUserIsFreelancer = Firestore.instance
        .collection('jobs')
        .where('freelancer', isEqualTo: widget.user.uid)
        .snapshots();

    Stream<QuerySnapshot> jobsWhereUserIsEmployer = Firestore.instance
        .collection('jobs')
        .where('employer', isEqualTo: widget.user.uid)
        .snapshots();

    //https://stackoverflow.com/questions/51214217/combine-streams-from-firestore-in-flutter#51214364
//    var streamGroup =
//        StreamGroup.merge([jobsWhereUserIsFreelancer, jobsWhereUserIsEmployer])
//            .asBroadcastStream();

//    return StreamBuilder<List<QuerySnapshot>>(stream: streamGroup, builder: (BuildContext context,
//      AsyncSnapshot<List<QuerySnapshot>> snapshotList){
//        if(!snapshotList.hasData){
//          return new Text('Loading...');
//        }
//        // note that snapshotList.data is the actual list of querysnapshots, snapshotList alone is just an AsyncSnapshot
//
//        int lengthOfDocs=0;
//        int querySnapShotCounter = 0;
//        snapshotList.data.forEach((snap){lengthOfDocs = lengthOfDocs + snap.documents.length;});
//        int counter = 0;
//        return ListView.builder(
//          itemCount: lengthOfDocs,
//          itemBuilder: (_,int index){
//            try{DocumentSnapshot doc = snapshotList.data[querySnapShotCounter].documents[counter];
//            counter = counter + 1 ;
//            return new Container(child: Text(doc.data["name"]));
//            }
//            catch(RangeError){
//              querySnapShotCounter = querySnapShotCounter+1;
//              counter = 0;
//              DocumentSnapshot doc = snapshotList.data[querySnapShotCounter].documents[counter];
//              counter = counter + 1 ;
//              return new Container(child: Text(doc.data["name"]));
//            }
//
//          },
//      );
//    };

    return StreamBuilder<QuerySnapshot>(
      stream: jobsWhereUserIsFreelancer,
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) return new Text('Error: ${snapshot.error}');
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
            return new Text('Loading...');
          default:
            return new ListView(
              children:
                  snapshot.data.documents.map((DocumentSnapshot document) {
                return new ListTile(
                  leading:
                      new Text('started ' + document['starttime'].toString()),
                  title: new Text(document['job']),
                  subtitle: new Text(document['duration'].toString()),
                );
              }).toList(),
            );
        }
      },
    );
  }

//  /**
//   * Testing Only
//   */
//  Widget _buildList(BuildContext context, List<Record> list) {
//    return ListView(
//      padding: const EdgeInsets.only(top: 20.0),
//      children: list.map((record) => _buildListItem(context, record)).toList(),
//    );
//  }
//
//  /**
//   * Testing only
//   */
//  Widget _buildListItemChild(BuildContext context, Record record) {
//    return Container(
//        decoration: BoxDecoration(),
//        child: ListTile(
//            title: Text(record.name),
//            onTap: () {
//              // log something
//              print(record.name);
//              navigateToSummary();
//            }));
//  }
//
//  /**
//   * Testing only
//   */
//  Widget _buildListItem(BuildContext context, Record record) {
//    return Padding(
//      key: ValueKey(record),
//      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
//      child: _buildListItemChild(context, record),
//    );
//  }

  //** Function: To Navigate to login_page.dart **********************************************
  void navigateToSummary() {
    Navigator.push(context, MaterialPageRoute(builder: (context) => Summary()));
  }

  //** Function: To Navigate to employee_current_job_page.dart **********************************************
  void navigateToCurrentJobPage() {
//    Navigator.push(context,
//        MaterialPageRoute(builder: (context) => EmployeeCurrentJobPage()));
//    Navigator.push(context,
//        MaterialPageRoute(builder: (context) => EmployerCurrentJobPage()));
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => InputJobDataPage()));
  }
}

class Record {
  // Vars
  final String name;

  Record(this.name); // Creators

  Record.fromMap(Map<String, dynamic> map)
      : assert(map['name'] != null),
        name = map['name'];

  // stringify
  @override
  String toString() => "Record<$name>";
}
