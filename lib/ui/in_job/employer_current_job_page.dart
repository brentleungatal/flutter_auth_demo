import 'dart:async';

import 'package:flutter/material.dart';
import 'package:qrcode_reader/qrcode_reader.dart';

class EmployerCurrentJobPage extends StatefulWidget {
  const EmployerCurrentJobPage({Key key}) : super(key: key);

  @override
  _CurrentJobState createState() => new _CurrentJobState();
}

class _CurrentJobState extends State<EmployerCurrentJobPage> {
  Future<String> qrCodeText;
  String qrCodeScanResult = '';

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Employer Current Job'),
      ),
      body: _buildBody(context),
      floatingActionButton: _buildQrCodeScannerFloatingActionButton(),
    );
  }

  Widget _buildBody(BuildContext buildContext) {
    return Stack(
      alignment: Alignment.center,
      children: <Widget>[
        // Navigate to QR code scanner.

        new Text(
          qrCodeScanResult != null ? qrCodeScanResult : 'No Code Scanned',
          style: TextStyle(color: Colors.black, fontSize: 18.0),
        )
      ],
    );
  }

  Future<String> futureString() async {
    return new QRCodeReader()
        .setAutoFocusIntervalInMs(200) // default 5000
        .setForceAutoFocus(true) // default false
        .scan();
  }

  Widget _buildQrCodeScannerFloatingActionButton() {
    return FloatingActionButton(
      tooltip: 'Add New Job', // used by assistive technologies
      child: Icon(Icons.add),
      onPressed: () {
        print('clicked FAB');

        var qrCodeReader = new QRCodeReader()
            .setAutoFocusIntervalInMs(200) // default 5000
            .setForceAutoFocus(true) // default false
            .scan()
            .then((string) {
          print('qrCodeReader scan result: ${string}'); // debug

          // cause a reload
          setState(() {
            qrCodeScanResult = string;
          });
        });
      },
    );
  }
}
