import 'package:flutter/material.dart';
import 'package:testing/ui/login/login_page.dart';

class WelcomePage extends StatefulWidget {
  _WelcomePageState createState() => new _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  //** Layout Widget *******************************************************************************
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text('Welcome'),
        ),
        body: new Container(
            padding: EdgeInsets.all(18.0),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                new RaisedButton(
                  child:
                  new Text('Login', style: new TextStyle(fontSize: 20.0)),
                  onPressed: navigateToLogin,
                ),
              ],
            )));
  }

  //** Function: To Navigate to login_page.dart **********************************************
  void navigateToLogin() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => LoginPage()));
  }
}
