import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:testing/model/job.dart';
import 'package:testing/service/firebase_firestore_service.dart';
import 'package:testing/ui/in_job/pre/job_input_page.dart';

class ListViewJob extends StatefulWidget {
  @override
  _ListViewJobState createState() => new _ListViewJobState();
}

class _ListViewJobState extends State<ListViewJob> {
  List<Job> items;
  FirebaseFirestoreService db = new FirebaseFirestoreService();

  StreamSubscription<QuerySnapshot> jobSub;

  @override
  void initState() {
    super.initState();

    items = new List();
    jobSub?.cancel();
    jobSub = db.getJobList().listen((QuerySnapshot snapshot) {
      final List<Job> jobs = snapshot.documents
          .map((documentSnapshot) => Job.fromMap(documentSnapshot.data))
          .toList();

      setState(() {
        this.items = jobs;
      });
    });
  }

  @override
  void dispose() {
    jobSub?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'grokonez Firestore Demo',
        home: Scaffold(
          appBar: AppBar(
            title: Text('grokonez Firestore Demo'),
            centerTitle: true,
            backgroundColor: Colors.blue,
          ),
          body: Center(
            child: ListView.builder(
                itemCount: items.length,
                padding: const EdgeInsets.all(15.0),
                itemBuilder: (context, position) {
                  return Column(
                    children: <Widget>[
                      Divider(height: 5.0),
                      ListTile(
                        title: Text(
                          '${items[position].title}',
                          style: TextStyle(
                            fontSize: 22.0,
                            color: Colors.deepOrangeAccent,
                          ),
                        ),
                        subtitle: Text(
                          '${items[position].description}',
                          style: new TextStyle(
                            fontSize: 18.0,
                            fontStyle: FontStyle.italic,
                          ),
                        ),
                        leading: Column(
                          children: <Widget>[
                            Padding(padding: EdgeInsets.all(10.0)),
                            CircleAvatar(
                                backgroundColor: Colors.blueAccent,
                                radius: 15.0,
                                child: Text(
                                  '${position + 1}',
                                  style: TextStyle(
                                    fontSize: 22.0,
                                    color: Colors.white,
                                  ),
                                )),
                            IconButton(
                              icon: const Icon(Icons.remove_circle_outline),
                              onPressed: () => _deleteJob(
                                  context, items[position], position),
                            )
                          ],
                        ),
                        onTap: () => _navigateToJob(context, items[position]),
                      )
                    ],
                  );
                }),
          ),
          floatingActionButton: FloatingActionButton(
              child: Icon(Icons.add), onPressed: () => _createNewJob(context)),
        ));
  }

  void _deleteJob(BuildContext context, Job job, int position) async {
    db.deleteJob(job.id).then((jobs) {
      setState(() {
        items.removeAt(position);
      });
    });
  }

  void _navigateToJob(BuildContext context, Job job) async {
    await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => JobScreen(job)),
    );
  }

  void _createNewJob(BuildContext context) async {
    await Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) =>
              JobScreen(Job(null, '', '', '', '', '', '', '', '', '', '', ''))),
    );
  }
}
