//For Both New user Registration and Existing User Login
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'package:testing/ui/user/activity_log.dart';

class LoginPage extends StatefulWidget {
  State<StatefulWidget> createState() => new _LoginPageState();
}

enum FormType {
  //** 2 form types: (1) User Login and (2) New User registration**
  login,
  register
}

class _LoginPageState extends State<LoginPage> {
  final formKey = new GlobalKey<FormState>();
  String _email, _name, _password, _role;
  FormType _formType = FormType.login;
  List<String> _roles = <String>['Freelancer', 'Employer'];

  //** Parent Layout Widget *******************************************************************************
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text('Freelancer App'),
        ),
        body: new Container(
          padding: EdgeInsets.all(18.0),
          child: new Form(
            key: formKey,
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: buildInputs() +
                  buildInputs2(), //Detail Input fields and Buttons defined in below Child Layouts.
            ),
          ),
        ));
  }

  //**** Child Layout: Email & Password (for both Login & Register) ************
  List<Widget> buildInputs() {
    return [
      new TextFormField(
        decoration: new InputDecoration(
            icon: const Icon(Icons.email), labelText: 'Email'),
        validator: (value) => value.isEmpty ? 'Email cannot be empty' : null,
        onSaved: (value) => _email = value,
      ),
      new TextFormField(
        decoration: new InputDecoration(
            icon: const Icon(Icons.security), labelText: 'Password'),
        obscureText: true,
        validator: (value) => value.isEmpty ? 'Password cannot be empty' : null,
        onSaved: (value) => _password = value,
      ),
    ];
  }

  //**** Child Layout: Other Input Fields and Buttons **************************
  List<Widget> buildInputs2() {
    if (_formType == FormType.login) {
      //***** For Login Page: Login Button *****
      return [
        new RaisedButton(
          child: new Text('Login', style: new TextStyle(fontSize: 20.0)),
          onPressed: validateAndSubmit,
        ),
        new FlatButton(
          child: new Text('Do not have an account? Create one Here.',
              style: new TextStyle(fontSize: 16)),
          onPressed: moveToRegister,
        )
      ];
    } else {
      //***** For Register Page: User fields and Register Button *****
      return [
        new TextFormField(
          decoration: new InputDecoration(
              icon: const Icon(Icons.person), labelText: 'Name'),
          validator: (value) => value.isEmpty ? 'Name cannot be empty' : null,
          onSaved: (value) => _name = value,
        ),
        new FormField(
          builder: (FormFieldState state) {
            return InputDecorator(
              decoration: InputDecoration(
                  icon: const Icon(Icons.work), labelText: 'Role'),
              child: new DropdownButtonHideUnderline(
                child: new DropdownButton(
                  value: _role,
                  isDense: true,
                  onChanged: (String newValue) {
                    setState(() {
                      _role = newValue;
                      state.didChange(newValue);
                    });
                  },
                  items: _roles.map((String value) {
                    return new DropdownMenuItem(
                      value: value,
                      child: new Text(value),
                    );
                  }).toList(),
                ),
              ),
            );
          },
        ),
        new RaisedButton(
          child: new Text('Create an account',
              style: new TextStyle(fontSize: 20.0)),
          onPressed: validateAndSubmit,
        ),
        new FlatButton(
          child: new Text('Already have an account? Login Here.',
              style: new TextStyle(fontSize: 16)),
          onPressed: moveToLogin,
        )
      ];
    }
  }
  //**************************************************************************************************

  //** Function: To validate user Input and Submit Form **********************************************
  Future<void> validateAndSubmit() async {
    final form = formKey.currentState;
    form.save();
    if (form.validate()) {
      try {
        if (_formType == FormType.login) {
          //** Login Function: SignIn to Firebase ****
          FirebaseUser user = await FirebaseAuth.instance
              .signInWithEmailAndPassword(email: _email, password: _password);
          print('Signed in: ${user.uid}');
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      Home(
                          user:
                          user))); //**Redirect to activity_log.dart after Login in
        } else {
          //** Register Function ****
          //** (1) Firebase: Authenication **
          FirebaseUser user = await FirebaseAuth.instance
              .createUserWithEmailAndPassword(
              email: _email, password: _password);
          print('Registered user: ${user.uid}');
          //** (2) Firebase Database: Add user the Collections "users" **
          Firestore.instance
              .collection('users')
              .document(user.uid)
              .setData({'name': '${_name}', 'role': '${_role}'});
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => Home(user: user)));
        }
      } catch (e) {
        print("Error: $e");
      }
    }
  }

  //** Function: To switch  formType (1) Login (2) Register **********************************************
  void moveToRegister() {
    formKey.currentState.reset(); //Clear form when switch
    setState(() {
      _formType = FormType.register;
    });
  }

  void moveToLogin() {
    formKey.currentState.reset(); //Clear form when switch
    setState(() {
      _formType = FormType.login;
    });
  }
}
